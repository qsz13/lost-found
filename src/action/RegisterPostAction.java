package action;

import com.opensymphony.xwork2.ActionSupport;
import entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.sql.Timestamp;

/**
 * Created by danielqiu on 5/25/14.
 */
public class RegisterPostAction extends ActionSupport {


    private String name;
    private String phone;
    private int id;
    private String username;
    private String password;
    private Timestamp time;

    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;


    public String execute() throws Exception
    {


        java.util.Date date = new java.util.Date();

        time = new Timestamp(date.getTime());



        sessionFactory = getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        UserEntity user = new UserEntity();
        user.setName(name);
        user.setCreateTime(time);
        user.setId(id);
        user.setPassword(password);
        user.setPhone(phone);
        user.setUsername(username);

        session.save(user);
        txn.commit();
        session.close();

        sessionFactory.close();





        return SUCCESS;
    }


    private SessionFactory getSessionFactory()
    {
        Configuration configuration = new Configuration();
        configuration.configure();
        serviceRegistry = new ServiceRegistryBuilder().applySettings(
                configuration.getProperties()).buildServiceRegistry();
        return configuration.buildSessionFactory(serviceRegistry);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
