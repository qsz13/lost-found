package action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.UserEntity;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by danielqiu on 5/25/14.
 */
public class LoginAction extends ActionSupport {


    private String username;
    private String password;


    private DataSource dataSource;
    private Connection connection;

    private UserEntity user;

    public String execute() throws Exception
    {
        InitialContext initialContext = new InitialContext();
        dataSource = (DataSource)initialContext.lookup("jdbc/lost_found");


        connection = dataSource.getConnection();


        if(passwordIsCorrect(username, password))
        {
            ActionContext.getContext().getSession().clear();
            Map<String, Object> session = ActionContext.getContext().getSession();
            session.put("loggedIn", "true");
            session.put("username",username);
            session.put("userID", this.user.getId());

            connection.close();

            return SUCCESS;
        }
        else
        {

            connection.close();
            return "failed";
        }



    }

    public boolean passwordIsCorrect(String username, String password) throws SQLException {
        String sql="select * from User WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,username);
        ResultSet rs = preparedStatement.executeQuery();
        String realPassword = null;
        this.user = new UserEntity();
        while (rs.next()) {
            realPassword = rs.getString("password");
            if(realPassword.equals(password))
            {
                this.user.setId(rs.getInt("id"));
                this.user.setUsername(rs.getString("username"));

                return true;
            }
            else
            {
                return false;
            }
        }
        return false;


    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
