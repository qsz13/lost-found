package action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.FoundEntity;
import entity.LostEntity;
import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;


/**
 * Created by danielqiu on 5/25/14.
 */
public class IndexAction extends ActionSupport {

    private DataSource dataSource;
    private Connection connection;
    private ArrayList<LostEntity> lostList = new ArrayList<LostEntity>();
    private ArrayList<FoundEntity> foundList = new ArrayList<FoundEntity>();
    private Boolean loggedIn = false;
    private String username;

    public String execute() throws Exception
    {
        InitialContext initialContext = new InitialContext();
        dataSource = (DataSource)initialContext.lookup("jdbc/lost_found");

        username = "";
        connection = dataSource.getConnection();

        queryForLost();
        queryForFound();


        Map<String, Object> sessionAttributes = ActionContext
                .getContext().getSession();
        System.out.print(sessionAttributes);

        if(sessionAttributes.get("user")!=null)
        {

            username = sessionAttributes.get("user").toString();
            if(sessionAttributes.get("loggedIn").toString() == "true")
            {
                this.loggedIn = true;
            }
            else
            {
                this.loggedIn = false;
            }

        }





        connection.close();
        return SUCCESS;
    }


    public void queryForLost() throws SQLException {
        String sql="select * from lost";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next())
        {

            LostEntity lostEntity = new LostEntity();
            lostEntity.setCategoryName(resultSet.getString("category_name"));
            lostEntity.setName(resultSet.getString("name"));
            lostEntity.setLostPlace(resultSet.getString("lost_place"));
            lostEntity.setLostTime(resultSet.getTimestamp("lost_time"));
            lostList.add(lostEntity);
        }

    }

    public void queryForFound() throws SQLException {
        String sql="select * from found";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next())
        {

            FoundEntity foundEntity = new FoundEntity();
            foundEntity.setCategoryName(resultSet.getString("category_name"));
            foundEntity.setName(resultSet.getString("name"));
            foundEntity.setFoundPlace(resultSet.getString("found_place"));
            foundEntity.setPublishTime(resultSet.getTimestamp("publish_time"));
//            System.out.println(foundEntity.getName());
            foundList.add(foundEntity);
        }
    }


    public ArrayList<LostEntity> getLostList() {
        return lostList;
    }

    public void setLostList(ArrayList<LostEntity> lostList) {
        this.lostList = lostList;
    }

    public ArrayList<FoundEntity> getFoundList() {
        return foundList;
    }

    public void setFoundList(ArrayList<FoundEntity> foundList) {
        this.foundList = foundList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
