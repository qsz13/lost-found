package action;

import com.opensymphony.xwork2.ActionSupport;
import entity.LostEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by danielqiu on 5/25/14.
 */
public class PostLostAction  extends ActionSupport {


    private String name;
    private String type;
    private String dateTimeString;
    private Date dateTime;
    private String place;
    private int userID;

    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


    public String execute() throws Exception
    {

        this.dateTime = formatter.parse(dateTimeString);
        long time = this.dateTime.getTime();
        System.out.print(this.dateTime);




        sessionFactory = getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction txn = session.beginTransaction();
        LostEntity lost = new LostEntity();
        lost.setName(name);
        lost.setCategoryName(type);
        lost.setPublishTime(new Timestamp(time));
        lost.setLostPlace(place);
        lost.setUserId(userID);

        session.save(lost);
        txn.commit();
        session.close();

        sessionFactory.close();

        return SUCCESS;
    }


    private SessionFactory getSessionFactory()
    {
        Configuration configuration = new Configuration();
        configuration.configure();
        serviceRegistry = new ServiceRegistryBuilder().applySettings(
                configuration.getProperties()).buildServiceRegistry();
        return configuration.buildSessionFactory(serviceRegistry);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }


}


