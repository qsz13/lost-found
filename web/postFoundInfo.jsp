<%@ page import="java.util.Map" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Post | Lost & Found Management System</title>

    <!-- Bootstrap css -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Flat UI css -->
    <link href="css/flat-ui.css" rel="stylesheet">

    <!-- Custom css -->
    <link rel="stylesheet" href="css/post.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <%
      String username = "unknow";
      String userID = "-1";
      Map<String, Object> sessionAttributes = ActionContext
              .getContext().getSession();

      if(sessionAttributes.get("username")!=null) {

          username = sessionAttributes.get("username").toString();
          userID = sessionAttributes.get("userID").toString();

          System.out.println(username);
      }

  %>


    <main class="login-screen">
      <div class="login-icon">
        <img src="images/icons/svg/paper-bag.svg" alt="Welcome th L&FMS">
        <h4>Complete the <small>information</small></h4>
      </div>
      <form class="login-form" name="loginForm" action="postfound" method="POST">
        <div class="form-group">
          <input type="text" class="form-control login-field" placeholder="Name" id="post-name" name="name" required autofocus>
          <label for="post-name" class="login-field-icon fui-search"></label>
        </div>
        <div class="form-group">
          <input type="text" class="form-control login-field" placeholder="Type" id="post-type" name="type">
          <label for="post-type" class="login-field-icon fui-list"></label>
        </div>
        <div class="form-group">
          <input type="text" class="form-control login-field" placeholder="Time" id="post-time" name="dateTimeString" required>
          <label for="post-time" class="login-field-icon fui-time"></label>
        </div>
        <div class="form-group">
          <input type="text" class="form-control login-field" placeholder="Place" id="post-place" name="place" required>
          <label for="post-place" class="login-field-icon fui-location"></label>
        </div>
          <input type="hidden" name="userID" value="<%=userID%>">
        <button type="submit" class="btn btn-primary btn-lg btn-block">Post</button>
      </form>
    </main>

    <!-- jQuery -->
    <script src="js/jquery-2.0.3.min.js"></script>
    
    <!-- Bootstrap js -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>